package basic.android.wifigpsbluetoothstatus;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext = this;
    TextView txtWifiStatus, txtGPSStatus, txtBluetoothStatus;
    Button statusWifi, statusGPS, statusBluetooth, btnWifiStart, btnGPSStart, btnBluetoothStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindingViews();
        setListeners();

    }


    public void bindingViews() {
        statusWifi = findViewById(R.id.statusWifi);
        statusGPS = findViewById(R.id.statusGPS);
        statusBluetooth = findViewById(R.id.statusBluetooth);

        txtWifiStatus = findViewById(R.id.txtWifiStatus);
        txtGPSStatus = findViewById(R.id.txtGPSStatus);
        txtBluetoothStatus = findViewById(R.id.txtBluetoothStatus);

        btnWifiStart = findViewById(R.id.btnWifiStart);
        btnGPSStart = findViewById(R.id.btnGPSStart);
        btnBluetoothStart = findViewById(R.id.btnBluetoothStart);

    }

    public void setListeners() {
        statusWifi.setOnClickListener(this);
        statusGPS.setOnClickListener(this);
        statusBluetooth.setOnClickListener(this);

        btnWifiStart.setOnClickListener(this);
        btnGPSStart.setOnClickListener(this);
        btnBluetoothStart.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view == statusWifi) {
            isInternetConnected(mContext);
        } else if (view == statusGPS) {
            isGPSActive(mContext);
        } else if (view == statusBluetooth) {
            isBluetoothActive();
        } else if (view == btnWifiStart) {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), 1);
        }else if (view == btnGPSStart) {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),2);
        }else if (view == btnBluetoothStart) {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS), 3);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1) {
            isInternetConnected(mContext);
        } else if (requestCode == 2) {
            isGPSActive(mContext);
        } else if (requestCode == 3) {
            isBluetoothActive();
         }
    }


    public void isInternetConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                txtWifiStatus.setText(getResources().getString(R.string.internet_by_wifi));

            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                txtWifiStatus.setText(getResources().getString(R.string.internet_by_data));
            }
        } else {
            // not connected to the internet
            txtWifiStatus.setText(getResources().getString(R.string.no_internet));
         }
    }

    public void isGPSActive(Context context) {

        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            txtGPSStatus.setText(getResources().getString(R.string.GPS_connection_enabled));
        } else {
            txtGPSStatus.setText(getResources().getString(R.string.GPS_connection_disabled));

        }
    }

    public void isBluetoothActive() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            txtBluetoothStatus.setText(getResources().getString(R.string.bluetooth_not_supported));
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                           txtBluetoothStatus.setText(getResources().getString(R.string.bluetooth_is_not_enabled));
            } else {
                txtBluetoothStatus.setText(getResources().getString(R.string.bluetooth_is_enabled));
            }

        }

    }

}